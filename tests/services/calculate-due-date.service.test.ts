import { expect } from 'chai';
import { CalculateDueDateService } from "../../src/sevices/calculate-due-date.service";
import { DateUtils } from '../../src/sevices/date.utils';

describe('CalculateDueDateService', function () {
  const dateUtils = new DateUtils();
  const calculator = new CalculateDueDateService(dateUtils);
  const getRndInteger = (min: number, max: number) => {
    return Math.floor(Math.random() * (max - min)) + min;
  }


  it('Throws_IfSubmitionDateIsNull', function () {
    expect(() => calculator.calculateDueDate(null, 1)).to.throw();
  });

  it('Throws_IfTurnaroundTimeIsNull', function () {
    expect(() => calculator.calculateDueDate(new Date(), null)).to.throw();
  });

  it('Throws_IfTurnaroundTimeIsNegative', function () {
    const turnaroundTime = getRndInteger(-20, 0);
    expect(() => calculator.calculateDueDate(new Date(), turnaroundTime)).to.throw();
  });

  it('CalculatesCorrect_WithinWorkingDay', function () {
    const hours = getRndInteger(9, 13);
    const minutes = getRndInteger(0, 60);
    const seconds = getRndInteger(0, 60);
    const date = new Date(2019, 11, 24, hours, minutes, seconds);
    const turnaroundTime = getRndInteger(1, 4);
    const expected = new Date(2019, 11, 24, hours + turnaroundTime, minutes, seconds);

    const result = calculator.calculateDueDate(date, turnaroundTime);

    expect(result.toString()).equal(expected.toString());
  });

  it('CalculatesCorrect_NextWorkingDay', function () {
    const hours = 16
    const minutes = getRndInteger(0, 60);
    const seconds = getRndInteger(0, 60);
    const date = new Date(2019, 11, 24, hours, minutes, seconds);
    const turnaroundTime = 3
    const expected = new Date(2019, 11, 25, 11, minutes, seconds);

    const result = calculator.calculateDueDate(date, turnaroundTime);

    expect(result.toString()).equal(expected.toString());
  });

  it('CalculatesCorrect_NextWorkingWeek', function () {
    const hours = 16
    const minutes = getRndInteger(0, 60);
    const seconds = getRndInteger(0, 60);
    const date = new Date(2019, 11, 24, hours, minutes, seconds);
    const turnaroundTime = 43
    const expected = new Date(2020, 0, 1, 11, minutes, seconds);

    const result = calculator.calculateDueDate(date, turnaroundTime);

    expect(result.toString()).equal(expected.toString());
  });

  it('CalculatesCorrect_StartAtWeekEnds', function () {
    const hours = 16
    const minutes = getRndInteger(0, 60);
    const seconds = getRndInteger(0, 60);
    const date = new Date(2019, 11, 28, hours, minutes, seconds);
    const turnaroundTime = 43
    const expected = new Date(2020, 0, 6, 12);

    const result = calculator.calculateDueDate(date, turnaroundTime);

    expect(result.toString()).equal(expected.toString());
  });
});