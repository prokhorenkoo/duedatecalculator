export class DateUtils {

    readonly dayStartHours = 9;
    readonly dayEndHours = 17;
    readonly daysInWeek = 7;
    readonly workingDays = [1, 2, 3, 4, 5];
    readonly workingWeekLength = this.workingDays.length;
    readonly workingDayLength = this.dayEndHours - this.dayStartHours;
    readonly millisecondsInHour = 1000 * 60 * 60;
    readonly millisecondsInDay = 24 * this.millisecondsInHour;


    getFullWeeksCount(turnaroundTimeHours: number): number {
        return Math.floor(turnaroundTimeHours / this.workingWeekLength / this.workingDayLength);
    }

    getFullDaysCount(turnaroundTimeHours: number): number {
        return Math.floor(turnaroundTimeHours / this.workingDayLength) % this.workingWeekLength;
    }

    getHours(turnaroundTimeHours: number): number {
        return turnaroundTimeHours - Math.floor(turnaroundTimeHours / this.workingDayLength) * this.workingDayLength;
    }

    getNextWorkingTime(date: Date): Date {
        const hours = date.getHours();
        if (!this.isWorkingDay(date) || hours >= this.dayEndHours) {
            date = this.getNextWorkingDay(date);
            return new Date(
                date.getFullYear(),
                date.getMonth(),
                date.getDate(),
                this.dayStartHours
            );
        } else if (hours < this.dayStartHours) {
            return new Date(
                date.getFullYear(),
                date.getMonth(),
                date.getDate(),
                this.dayStartHours
            );
        }
        return new Date(date);
    }
    toBeginWorkingDay(date: Date): Date {
        return new Date(
            date.getFullYear(),
            date.getMonth(),
            date.getDate(),
            this.dayStartHours
        );
    }
    getNextWorkingDay(date: Date): Date {
        date = new Date(date);
        do {
            date = new Date(date.getTime() + this.millisecondsInDay);
        } while (!this.isWorkingDay(date))
        return date;
    }

    addWeeks(date: Date, weeks: number): Date {
        date = new Date(date.getTime() + weeks * this.daysInWeek * this.millisecondsInDay);
        return date;
    }

    leftWorkingTime(date: Date): number {
        const end = new Date(
            date.getFullYear(),
            date.getMonth(),
            date.getDate(),
            this.dayEndHours
        );

        return (end.getTime() - date.getTime()) / this.millisecondsInHour;
    }

    private isWorkingDay(date: Date): boolean {
        return this.workingDays.indexOf(date.getDay()) > -1;
    }


    toMilliseconds(hours: number): number {
        return hours * this.millisecondsInHour;
    }


}