import { DateUtils } from "./date.utils";

export class CalculateDueDateService {
    constructor(protected dateUtils: DateUtils) { }

    public calculateDueDate(submitionDate: Date, turnaroundTimeHours: number): Date {
        if (typeof (submitionDate) === 'undefined' || submitionDate === null ||
            typeof (turnaroundTimeHours) === 'undefined' || turnaroundTimeHours === null)
            throw new Error("Argument cannot ce null");

        if (turnaroundTimeHours < 0)
            throw new Error("turnaroundTimeHours connot be negative");

        let date: Date = this.dateUtils.getNextWorkingTime(submitionDate);

        const left = this.dateUtils.leftWorkingTime(date);
        let hours = this.dateUtils.getHours(turnaroundTimeHours);
        const days = this.dateUtils.getFullDaysCount(turnaroundTimeHours);
        const weeks = this.dateUtils.getFullWeeksCount(turnaroundTimeHours);

        if (left <= hours) {
            date = this.dateUtils.getNextWorkingDay(date);
            date = this.dateUtils.toBeginWorkingDay(date);
            hours -= left;
        }

        date = new Date(date.getTime() + this.dateUtils.toMilliseconds(hours));

        for (let i = 0; i < days; i++)
            date = this.dateUtils.getNextWorkingDay(date);

        date = this.dateUtils.addWeeks(date, weeks);

        return date;
    }
}