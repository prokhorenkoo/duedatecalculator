import express from 'express';
import { DateUtils } from './sevices/date.utils';
import { CalculateDueDateService } from './sevices/calculate-due-date.service';

console.log("staring on 8080");

const app = express();
const dateUtils = new DateUtils();
const calculator = new CalculateDueDateService(dateUtils);

app.get('/', function (req, res) {

    // Retrieve the tag from our URL path
    const submitionDate = new Date(req.query.submitionDate)
    const turnaroundTimeHours = +req.query.turnaroundTimeHours
    const result = calculator.calculateDueDate(submitionDate, turnaroundTimeHours);

    res.write(result.toString());
    res.end();
});

let server = app.listen(8080, function () {
    console.log('Server is listening on port 8080')
});
